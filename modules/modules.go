package modules

import (
	_ "dzhcms/modules/dzhCommon"
	_ "dzhcms/modules/dzhMember"

	_ "dzhcms/modules/dzhCms"

	_ "github.com/cool-team-official/cool-admin-go/modules/base"
	_ "github.com/cool-team-official/cool-admin-go/modules/dict"
	_ "github.com/cool-team-official/cool-admin-go/modules/space"
	_ "github.com/cool-team-official/cool-admin-go/modules/task"
)
