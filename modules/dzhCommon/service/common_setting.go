package service

import (
	"dzhcms/modules/dzhCommon/model"

	"github.com/cool-team-official/cool-admin-go/cool"
)

type CommonSettingService struct {
	*cool.Service
}

func NewCommonSettingService() *CommonSettingService {
	return &CommonSettingService{
		&cool.Service{
			Model: model.NewCommonSetting(),
		},
	}
}
