package admin

import (
	"dzhcms/modules/dzhCms/service"

	"github.com/cool-team-official/cool-admin-go/cool"
)

type CmsLotteryController struct {
	*cool.Controller
}

func init() {
	var cms_lottery_controller = &CmsLotteryController{
		&cool.Controller{
			Perfix:  "/admin/cms/lottery",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewCmsLotteryService(),
		},
	}
	// 注册路由
	cool.RegisterController(cms_lottery_controller)
}
