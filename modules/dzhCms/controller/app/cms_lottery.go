package app

import (
	v1 "dzhcms/api/v1"
	"dzhcms/modules/dzhCms/service"

	"github.com/cool-team-official/cool-admin-go/cool"
	"github.com/gogf/gf/v2/frame/g"
)

type CmsLotteryController struct {
	*cool.Controller
}

func init() {
	var cms_lottery_controller = &CmsLotteryController{
		&cool.Controller{
			Perfix:  "/app/cms/lottery",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewCmsLotteryService(),
		},
	}
	// 注册路由
	cool.RegisterController(cms_lottery_controller)
}

// 生成开奖号码
func (c *CmsLotteryController) GenerateNumber(ctx g.Ctx, req *v1.GenerateNumberReq) (res *cool.BaseRes, err error) {

	data, err := service.NewCmsLotteryService().GenerateNumber(ctx)
	if err != nil {
		return
	}
	res = cool.Ok(data)
	return
}

// 获取当前开奖数据
func (c *CmsLotteryController) GetNowLottery(ctx g.Ctx, req *v1.GetNowLotteryReq) (res *cool.BaseRes, err error) {

	data, err := service.NewCmsLotteryService().GetNowLottery(ctx)
	if err != nil {
		return
	}
	res = cool.Ok(data)
	return
}
