package model

import (
	"github.com/cool-team-official/cool-admin-go/cool"
)

const TableNameCmsLottery = "dzh_cms_lottery"

// CmsLottery mapped from table <cms_lottery>
type CmsLottery struct {
	*cool.Model
	LotteryTime   string `gorm:"column:lotteryTime;not null;comment:开奖时间" json:"lotteryTime"`
	CurrentNumber string `gorm:"column:currentNumber;not null;comment:当前期号" json:"currentNumber"`
	LotteryNumber string `gorm:"column:lotteryNumber;not null;comment:开奖号码" json:"lotteryNumber"`
	NumTrend      string `gorm:"column:numTrend;not null;comment:号码走势" json:"numTrend"`
	Count         string `gorm:"column:count;not null;comment:当天期号" json:"count"`
	Status        *int32 `gorm:"column:status;not null;type:int;default:1" json:"status"` // 状态 0:禁用 1：启用
}

// TableName CmsLottery's table name
func (*CmsLottery) TableName() string {
	return TableNameCmsLottery
}

// GroupName CmsLottery's table group
func (*CmsLottery) GroupName() string {
	return "default"
}

// NewCmsLottery create a new CmsLottery
func NewCmsLottery() *CmsLottery {
	return &CmsLottery{
		Model: cool.NewModel(),
	}
}

// init 创建表
func init() {
	cool.CreateTable(&CmsLottery{})
}
