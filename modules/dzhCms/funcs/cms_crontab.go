package funcs

import (
	"dzhcms/modules/dzhCms/service"
	"dzhcms/utility/logger"

	"github.com/cool-team-official/cool-admin-go/cool"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
)

type CmsCrontab struct{}

func (f *CmsCrontab) Func(ctx g.Ctx, param string) (err error) {

	logger.Info(ctx, "定时开始")

	data, err := service.NewCmsLotteryService().GenerateNumber(ctx)

	logger.Infof(ctx, "生成成功：%v", gjson.MustEncodeString(data))

	return
}

// IsSingleton
func (f *CmsCrontab) IsSingleton() bool {
	return true
}

// IsAllWorker
func (f *CmsCrontab) IsAllWorker() bool {
	return false
}

// init
func init() {
	cool.RegisterFunc("CmsCrontab", &CmsCrontab{})
}
