package util

import (
	"context"
	"math/rand"
	"time"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/text/gstr"
)

func CnTranEn(ctx context.Context, str string) (data string) {
	data = "Chinese girl national team champion"
	return
}

// 从1到 num 中随机生成  n  个不重复的数字
func RandCountArr(num int, n int) []int {
	var pools g.SliceInt
	// 使用循环生成数字序列
	for i := 1; i <= num; i++ {
		pools = append(pools, i)
	}
	// g.Dump(pools)

	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)
	// 随机打乱切片顺序
	random.Shuffle(len(pools), func(i, j int) {
		pools[i], pools[j] = pools[j], pools[i]
	})

	// 选择前count个数字
	selectedNumbers := pools[:n]

	return selectedNumbers
}

// 切片转字符串
func ArrToStr(arr []int, step string) string {
	return gstr.JoinAny(arr, step)
}
