package main

import (
	_ "dzhcms/internal/packed"

	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/os/gtime"

	_ "github.com/cool-team-official/cool-admin-go/contrib/drivers/mysql"

	_ "dzhcms/modules"

	"github.com/gogf/gf/v2/os/gctx"

	"dzhcms/internal/cmd"

	_ "github.com/cool-team-official/cool-admin-go/contrib/files/local"
)

func main() {
	// gres.Dump()

	cmd.Main.Run(gctx.New())

	// 设置进程全局时区
	err := gtime.SetTimeZone("Asia/Tokyo")
	if err != nil {
		panic(err)
	}
}
